# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.1.2](https://gitlab.com/biomedit/sett-rs/compare/0.1.1...0.1.2) (2021-08-11)


### Features

* **sftp:** make upload buffer size configurable ([07e594b](https://gitlab.com/biomedit/sett-rs/commit/07e594b95452d0097f4ce780bd5d83577c230b7b))

### [0.1.1](https://gitlab.com/biomedit/sett-rs/compare/0.1.0...0.1.1) (2021-08-04)


### Features

* **sftp:** log uploaded files size ([2cd9d11](https://gitlab.com/biomedit/sett-rs/commit/2cd9d11b9167a03646b8c2097f5ffe5cbd873141))


### Bug Fixes

* remove needless_borrow reported by clippy ([3236462](https://gitlab.com/biomedit/sett-rs/commit/32364628a498a928f06ff6513d92882ad8772e82))
* replace deprecated macro text_signature with new pyo3(text_signature) ([fabefb0](https://gitlab.com/biomedit/sett-rs/commit/fabefb03cacb423c11dc7cdb0ec4424031266bf4))

## 0.1.0 (2021-08-03)


### Bug Fixes

* **sftp:** add upload progress ([d2576e4](https://gitlab.com/biomedit/sett-rs/commit/d2576e412daffb4ea76e1be0bde6142dcba79ea6))

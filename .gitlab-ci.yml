stages:
  - test
  - build
  - prepare-release
  - release-gitlab
  - deploy
  - external

code-style:
  stage: test
  image: rust
  before_script:
    - rustup component add rustfmt
    - rustup component add clippy
  script:
    - cargo fmt -- --check
    - cargo clippy

test:
  stage: test
  image: rust
  script:
    - cargo test

build-linux:
  stage: build
  image:
    name: konstin2/maturin
    entrypoint: ["/bin/bash", "-c"]
  script:
    - maturin build --release --no-sdist --strip -m sett-rs/Cargo.toml
  artifacts:
    paths:
      - target/wheels/*.whl

build-windows:
  stage: build
  tags:
    - shared-windows
    - windows
    - windows-1809
  before_script:
    - |
      Invoke-WebRequest -Uri "https://win.rustup.rs" -OutFile "rustup-init.exe"
      .\rustup-init.exe -y
      $env:PATH+=";PATH=%USERPROFILE%\.cargo\bin"
    - |
      choco install python --version=3.6.8 -my
      choco install python --version=3.7.9 -my
      choco install python --version=3.8.10 -my
      choco install python --version=3.9.5 -my
      $env:PATH+=";C:\Python36;C:\Python36\Scripts;C:\Python37;C:\Python37\Scripts;C:\Python38;C:\Python38\Scripts;C:\Python39;C:\Python39\Scripts"
      $env:Path = [System.Environment]::GetEnvironmentVariable("Path","Machine") + ";" + [System.Environment]::GetEnvironmentVariable("Path","User")
      python -m pip install --upgrade maturin
  script:
    - maturin.exe build --release --no-sdist --strip -m sett-rs\Cargo.toml
  artifacts:
    paths:
      - target\wheels\*.whl

pypi:
  stage: deploy
  image: python:3
  variables:
    TWINE_USERNAME: $PYPI_USERNAME
    TWINE_PASSWORD: $PYPI_TOKEN
  before_script:
    - pip install twine wheel
  script:
    - twine upload target/wheels/*.whl
  only:
    - tags

release-gitlab:
  stage: release-gitlab
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  script:
    - release-cli create --name="Release $CI_COMMIT_TAG" --description="See [CHANGELOG.md](https://gitlab.com/${CI_PROJECT_PATH}/-/blob/master/CHANGELOG.md)." --tag-name "$CI_COMMIT_TAG" --ref "$CI_COMMIT_SHA"
  only:
    - tags

prepare-release:
  stage: prepare-release
  image: node:latest
  before_script:
    # Deploy Key: Public key with write access added in Settings -> Repository -> Deploy Keys
    #             Private key added as `SSH_PRIVATE_KEY` in Settings -> CI / CD -> Variables
    - "which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )"
    - eval $(ssh-agent -s)
    - ssh-add <(echo "$SSH_PRIVATE_KEY")
    - mkdir -p ~/.ssh
    - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'
    - ssh -T git@gitlab.com
    - git config user.name "GitLab CI"
    - git config user.email "build@gitlab.com"
    - git fetch --tags # Workaround for https://gitlab.com/gitlab-org/gitaly/issues/2221
    - npm install -g standard-version@9.3
  script:
    - standard-version
    - git push --tags "git@gitlab.com:${CI_PROJECT_PATH}.git" "HEAD:master"
    - git push --tags "git@gitlab.com:${CI_PROJECT_PATH}.git" "HEAD:dev"
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - if: '$CI_COMMIT_MESSAGE =~ /.?chore\(release\): /'
      when: never
    - if: '$CI_COMMIT_BRANCH == "master"'

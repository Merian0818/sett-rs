# sett-rs

Python bindings for sett internals implemented in Rust.

## Build

Building Python packages

### Development

Build and install directly in the current virtual env.

```bash
maturin develop -m sett-rs/Cargo.toml --release
```

### Production

Build manylinux wheels for production.

```bash
docker run --rm -v $(pwd):/io konstin2/maturin build --release --no-sdist --strip -m sett-rs/Cargo.toml
```

## Examples

### File transfers

```bash
cd sett-rs/examples
# Set up an sftp server for testing (public key authentication)
docker run -v ${HOME}/.ssh/id_rsa.pub:/home/foo/.ssh/keys/id_rsa.pub:ro -p 2220:22 -d atmoz/sftp foo::1001
# Run speed test
python speedtest.py
```

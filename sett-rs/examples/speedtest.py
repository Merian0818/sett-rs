import functools
import logging
import os
import time

import sett_rs

FORMAT = "%(levelname)s %(name)s %(asctime)-15s %(filename)s:%(lineno)d %(message)s"
logging.basicConfig(format=FORMAT)
logging.getLogger().setLevel(logging.DEBUG)

source_path = "./speedtest.py"
host = "127.0.0.1:2220"
user = "foo"
destination = "upload"

source = os.path.expanduser(source_path)
source_size_mb = os.path.getsize(source) / 1_000_000


def timer(fn):
    @functools.wraps(fn)
    def wrapper(*args, **kwargs):
        tic = time.perf_counter()
        value = fn()
        tac = time.perf_counter()
        delta = tac - tic
        print(
            f"{fn.__name__} took {delta:.2f} s, speed {source_size_mb / delta:.2f} MB/s"
        )
        return value

    return wrapper


class Progress:
    """Interface for progress objects."""

    def update(self, completed_fraction: float) -> None:
        print(completed_fraction)


@timer
def sftp_upload():
    progress = Progress()
    sett_rs.sftp_upload([source], host, user, destination, None, None, None, progress)


if __name__ == "__main__":
    sftp_upload()

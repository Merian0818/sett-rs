pub fn to_human_readable_size(size: u64) -> String {
    let prefix = ["", "k", "M", "G", "T", "P", "E"];
    let exp = 1024.0;
    let log_value = if size > 0 {
        (size as f32).log(exp) as usize
    } else {
        0
    };
    format!(
        "{:.prec$} {}B",
        size as f32 / exp.powi(log_value as i32),
        prefix[log_value],
        prec = if log_value > 0 { 2 } else { 0 }
    )
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_to_human_readable_size() {
        let sizes = [
            (0u64, "0 B"),
            (1023u64, "1023 B"),
            (9253u64, "9.04 kB"),
            (3771286u64, "3.60 MB"),
            (8363220129, "7.79 GB"),
            (7856731783569, "7.15 TB"),
            (4799178968842384, "4.26 PB"),
            (3424799178968842384, "2.97 EB"),
        ];
        for (size, expected) in sizes {
            assert_eq!(to_human_readable_size(size), expected);
        }
    }
}

use std::io::{BufReader, Read, Write};
use std::net::TcpStream;
use std::path::Path;
use std::{fs, fs::File};

use chrono::Utc;
use log::{debug, info};
use ssh2::{RenameFlags, Session};

use crate::error::Error;
use crate::utils;

fn connect_with_agent(username: &str, session: &mut Session) -> Result<(), Error> {
    let mut agent = session.agent()?;
    agent.connect()?;
    agent.list_identities()?;
    let identities = agent.identities()?;
    let key = &identities
        .iter()
        .find(|i| matches!(agent.userauth(username, i), Ok(())));
    agent.disconnect()?;
    key.ok_or(Error(String::from("Agent authentication failed")))?;
    Ok(())
}

fn get_combined_file_size(files: &[&str]) -> Result<u64, Error> {
    let mut sizes = Vec::new();
    for f in files {
        sizes.push(fs::metadata(f)?.len());
    }
    Ok(sizes.iter().sum())
}

pub struct Remote<'a> {
    pub host: &'a str,
    pub username: &'a str,
    pub path: &'a str,
    pub envelope_dir: Option<&'a str>,
    pub key_path: Option<&'a str>,
    pub key_password: Option<&'a str>,
}

pub fn upload(
    files: &[&str],
    remote: &Remote,
    progress: Option<impl Fn(f32) -> Result<(), Error>>,
    buf_size: Option<usize>, // Buffer size for uploading files
) -> Result<(), Error> {
    let buf_size = buf_size.unwrap_or(65536);
    const PROGRESS_PRECISION: f32 = 100.0; // 100.0 means 2 decimal places
    const DATETIME_FORMAT: &str = "%Y%m%dT%H%M%S_%f";

    let tcp = TcpStream::connect(remote.host)?;
    let mut session = Session::new()?;
    session.set_tcp_stream(tcp);
    session.handshake()?;
    // TODO should we check known hosts?
    if let Some(key) = remote.key_path {
        session.userauth_pubkey_file(remote.username, None, Path::new(key), remote.key_password)?;
    } else {
        debug!("No SSH key used. Using SSH Agent");
        connect_with_agent(remote.username, &mut session)?;
    }
    let default_envelope_dir = Utc::now().format(DATETIME_FORMAT).to_string();
    let upload_dir_str = format!(
        "{}/{}",
        remote.path,
        remote.envelope_dir.unwrap_or(&default_envelope_dir)
    );
    let upload_dir = Path::new(&upload_dir_str);

    let sftp = session.sftp()?;
    // TODO mkdir will fail if parent is missing, should it be recursive?
    sftp.mkdir(upload_dir, 0o755)?;

    let total_size = get_combined_file_size(files)?;
    let mut transferred_size = 0;
    let mut current_progress = 0.0;
    let mut buffer = vec![0; buf_size];
    for f in files {
        let source_path = Path::new(f);
        let name = if let Some(p) = source_path.file_name() {
            p.to_string_lossy().into_owned()
        } else {
            Utc::now().format(DATETIME_FORMAT).to_string()
        };
        let remote_path_part = format!("{}/{}.part", &upload_dir_str, &name);
        let remote_path_final = format!("{}/{}", &upload_dir_str, &name);

        let fin = File::open(source_path)?;
        let mut reader = BufReader::with_capacity(buf_size, fin);
        let mut fout = sftp.create(Path::new(&remote_path_part))?;
        loop {
            let bytes_read = reader.read(&mut buffer)?;
            if bytes_read == 0 {
                break;
            }
            fout.write_all(&buffer[..bytes_read])?;
            transferred_size += bytes_read;
            // Update progress only when the increase is bigger than the precision
            if let Some(progress_callback) = &progress {
                let rounded_fraction =
                    (transferred_size as f32 / total_size as f32 * PROGRESS_PRECISION).round()
                        / PROGRESS_PRECISION;
                if rounded_fraction > current_progress {
                    current_progress = rounded_fraction;
                    progress_callback(current_progress)?;
                }
            }
        }
        sftp.rename(
            Path::new(&remote_path_part),
            Path::new(&remote_path_final),
            Some(RenameFlags::ATOMIC | RenameFlags::NATIVE),
        )?;
        info!(
            "Successfully transferred {} (size: {})",
            &f,
            utils::to_human_readable_size(fs::metadata(f)?.len())
        );
    }
    let done_marker_file = format!("{}/{}", upload_dir_str, "done.txt");
    sftp.create(Path::new(&done_marker_file))?;

    Ok(())
}

#![allow(clippy::unnecessary_wraps)]
use pyo3::exceptions::PyRuntimeError;
use pyo3::prelude::*;
use pyo3::wrap_pyfunction;

pub mod error;
pub mod sftp;
pub mod utils;

use crate::error::Error;
use crate::sftp::Remote;

fn to_pyerror<E: std::fmt::Debug + Sized>(e: E) -> PyErr {
    PyRuntimeError::new_err(format!("{:?}", e))
}

/// Upload files to an SFTP server
#[allow(clippy::too_many_arguments)]
#[pyfunction]
#[pyo3(
    text_signature = "(files, host, username, destination_dir, envelope_dir, pkey, pkey_password, progress, /)"
)]
fn sftp_upload(
    py: Python,
    files: Vec<&str>,
    host: &str,
    username: &str,
    destination_dir: &str,
    envelope_dir: Option<&str>,
    pkey: Option<&str>,
    pkey_password: Option<&str>,
    progress: Option<PyObject>,
    buf_size: Option<usize>,
) -> PyResult<()> {
    let progress_callback = progress.map(|p| {
        move |x| {
            p.call_method1(py, "update", (x,))
                .map_err(|e| Error(format!("{:?}", e)))?;
            Ok(())
        }
    });
    let remote = Remote {
        host,
        username,
        path: destination_dir,
        envelope_dir,
        key_path: pkey,
        key_password: pkey_password,
    };
    sftp::upload(&files, &remote, progress_callback, buf_size).map_err(to_pyerror)
}

#[pymodule]
fn sett_rs(_py: Python, m: &PyModule) -> PyResult<()> {
    pyo3_log::init();
    m.add_function(wrap_pyfunction!(sftp_upload, m)?)?;
    Ok(())
}

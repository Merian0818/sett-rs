const versionPrefix = "version = \"";

function findVersion(versionPrefix, contents) {
  const start = contents.indexOf(versionPrefix) + versionPrefix.length;

  let end = contents.indexOf("\"", start);
  if (!end) {
    throw new Error("end of version not found!");
  }

  return contents.substring(start, end);
}

module.exports.readVersion = function (contents) {
  return findVersion(versionPrefix, contents);
};

module.exports.writeVersion = function (contents, version) {
  const previousVersion = findVersion(versionPrefix, contents);
  return contents.replace(
    versionPrefix + previousVersion,
    versionPrefix + version
  );
};
